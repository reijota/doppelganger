<?php
    #Poder usar la función islike en todo el proyecto como función nativa de php
    if (!function_exists('isLike')) {
        function isLike($haystack, $needle)
        {            
           return \core\funcion::isLike($haystack, $needle);
        }
	}
	
    if(!function_exists('triggerJson')){
        function triggerJson($valor){            
           return \core\funcion::triggerJson($valor);
        }
	}
	
	if(!function_exists('echoJson')){
        function echoJson($valor){
           return \core\funcion::echoJson($valor);
        }
    }
?>