<?php
	spl_autoload_register(
		function ($nombre_clase) { 
			//Voy un nivel hacia atras desde el directorio del fichero (no el de ejecución del script)
			$rutacompleta = str_replace(basename(__DIR__),'', __DIR__);
			//necesito saber si se trata de un namespace ya que sería diferente llamada
			$nombre = explode('\\', $nombre_clase);
			//Si tengo más de un elemento implica que se trata de un namespace
			if (count($nombre) > 1){
				$fullpath = $rutacompleta.'modulos/'.strtolower($nombre["0"]).'/'.strtolower($nombre["1"]).'.class.php';
			}else{
				$fullpath = $rutacompleta.'modulos/'.strtolower($nombre["0"]).'/'.strtolower($nombre["0"]).'.class.php';
			}
			//echo($fullpath);
			if(file_exists($fullpath)){
				require_once($fullpath);
			} else {
				throw new Exception('AUTOLOAD => No localizado el fichero "' . $fullpath . '".');
			}
		}
	);

?>