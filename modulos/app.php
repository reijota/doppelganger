<?php
#Ruta
    $ruta = str_replace(basename(__DIR__),'', __DIR__);
    #Cargar Config
    require_once($ruta.'modulos/config.php');
    #Cargar Autoload Vendor
    require_once($ruta.'vendor/autoload.php');
    #Cargar Autoload SIGO
    require_once($ruta.'modulos/autoload.php');
    #Cargar Helpers de SIGO
    require_once($ruta.'modulos/helper.php');
?>