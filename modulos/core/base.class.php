<?php namespace core;

	//Me posiciono en el directorio actual
  	require_once dirname(__FILE__). '/../config.php';

	class base {
		#Datos de Mysql
		public $mysql_host = DB_HOST;
		public $mysql_user  = DB_USER;
		public $mysql_password = DB_PASS;
		public $mysql_bd = DB_NAME;
		public $slow_query = SLOW_QUERY;
		protected $conexion;
		protected $idconexion;

        /**
         * Conectar al mysql
         * 
         * Las datos se definen en el config
         */
		protected function conectar() {
			//Realizamos la conexion
			$this->idconexion = new \mysqli($this->mysql_host, $this->mysql_user, $this->mysql_password);
			if ($this->idconexion->connect_errno) {
				printf("Falló la conexión: %s\n", $this->idconexion->connect_error);
				return 0;
			}
			//seleccionamos la base de datos
			if ($this->idconexion->select_db($this->mysql_bd)) {
				$this->Error = "Imposible establecer la conexion a la Base de Datos ".$this->mysql_bd;
				return 0;
			}
			//Si todo ha funcionado ok, devolvemos el idconexion
			$this->idconexion->set_charset(DB_CHARSET);
			$this->idconexion->query("SET NAMES ".DB_CHARSET);
			// NO afectará a $mysqli->real_escape_string();
			$this->idconexion->query("SET CHARACTER SET ".DB_CHARSET);
			return $this->idconexion;
		}

		/**
         * Cambiar el servidor de la base de datos a la cual conectarte
         * 
         * @param string $host : IP del servidor Mysql
		 * @param string $usuario: usuario de mysql
		 * @param string $password : contraseña de mysql
         */
		public function setServerDatabase($host, $usuario, $password){
			$this->mysql_host = $host;
			$this->mysql_user = $usuario;
			$this->mysql_password = $password;
		}
		
		/**
		 * Cierre el enlace a la base de datos de mysql
		 * 
		 */
		protected function desconectar(){
			$this->idconexion->close();
		}

		/**
		 * Obtiene el charset de la base de datos
		 */
		public function getCharset(){
			self::conectar();
			$str = $this->idconexion->character_set_name();
			self::desconectar();
			return $str;
		}

		/**
		 * Escapa los caracteres especiales de una cadena para usarla en una sentencia SQL
		 * 
		 * @param string: $valor : string del cual escapar
		 * @return string : string ya "escapado"
		*/
		public function escapeString($valor){
			self::conectar();
			$str = $this->idconexion->real_escape_string($valor);
			self::desconectar();
			return $str;
		}

		/**
		 * Se usa para actualizacion/eliminacion/creacion a la base de datos
		 * Es decir, para consultas de UPDATE, INSERT, DELETE
		 * En consultas de INSERT devuelve el ultimo id insertado
		 * En consultas de UPDATE/DELETE devuelve el número de filas afectadas
		 * 
		 * @param $sql: query a lanzar
		 * @return: integer o vacio: filas afectadas o id insertado
		 */
		public function consultaSimple($sql){
			self::conectar();
			$this->idconexion->set_charset(DB_CHARSET);
			$this->idconexion->query($sql);
			$id_insertado = '';
			if (!empty($this->idconexion->insert_id)) {
				$id_insertado = $this->idconexion->insert_id;
			}else{
				$id_insertado = $this->idconexion->affected_rows;
			}
			if ($this->idconexion->error){
				self::triggerError($this->idconexion->error, $sql);
			}
			self::desconectar();
			return $id_insertado;
		}


		/**
		 * Esta función la utilizo para guardar valores directamente en tabla sin necesidad
		 * de addslashes, ni htmlentities, ni nada similar, sin embargo SOLO vale con un unico valor
		 * @param string: tabla que modificar
		 * @param string: campo que modificar
		 * @param string: valor que introducir en el campo
		 * @param string: resto de la sentencia WHERE
		 */
		public function updateBlob($tabla, $campo, $valor, $consultaWhere){
			self::conectar();
			$this->idconexion->set_charset(DB_CHARSET);
			if (!empty($consultaWhere)){
				$sql = "UPDATE {$tabla} SET $campo = ? WHERE {$consultaWhere} ";
				$sentencia = $this->idconexion->prepare($sql);
			}else{
				$sql = "UPDATE {$tabla} SET $campo = ? ";
				$sentencia = $this->idconexion->prepare($sql);
			}
			#Venculo el parametro como string (el blob es el destino)
			if (!$sentencia->bind_param("s", $valor)) {
				trigger_error( " la vinculación de parámetros: (" . $sentencia->errno . ") " . $sentencia->error );
			}
			#Ejecuto
			if (!$sentencia->execute()) {
				self::triggerError($sentencia->error, "Fallo en updateBlob para la tabla {$tabla} el campo {$campo} con el where {$consultaWhere} ");
			}
			$sentencia->close();
			self::desconectar();
		}

        /**
         * Devuelve un array multi-dimensional con los datos de la query ejecutada
		 * "Tal cual" viene en tabla llega el array
         * 
         * @param string $sql: query sql a ejecutar
         * @param array: devuelve un array con los datos, en caso de no obtener datos devuelve un array vacio
         */
		public function consultaRetorno($sql){
			self::conectar($sql);
			if ($this->slow_query == '1') {
				$queryTime1 = $this->getTimestamp();
			}
			$this->idconexion->set_charset(DB_CHARSET);
			$rs_query = $this->idconexion->query($sql);
			$resultado = array();
			if ($rs_query == false){ 
				#Impedir Notice => Trying to get property 'error' of non-object 
				if (is_object($rs_query)){
					$resultado = $rs_query->error;
				}else{
					self::triggerError($this->idconexion->error, $sql);
				}
			}else{
				while ( $fl_query = $rs_query->fetch_array(MYSQLI_ASSOC)){
					$resultado[] = $fl_query;
				}
			}
			if ($this->slow_query == '1') {
				$queryTime2 = $this->getTimestamp();
				$logger = new logger();
				$logger->getTiempoQuery($queryTime1, $queryTime2, $sql);
				unset($logger);
			}
			self::desconectar();
			return $resultado;
		}

		/**
         * Devuelve los datos una columna (primera por default) un array "NO" multi dimensional
		 * Devolviendo los datos como clave=>valor. Es decir en vez de acceder al primer elemento como $array["0"]["campo"]
		 * se accede como $array["0"] al primer elemento
         * 
         * @param string $sql: query sql a ejecutar
         * @param string: devuelve un string con el datos solicitado, en caso de no encontrar nada devuelve un string vacio
         */
		public function consultaColumnaUnica($sql, $numeroColumna = '0'){
			self::conectar($sql);
			if ($this->slow_query == '1') {
				$queryTime1 = $this->getTimestamp();
			}
			$this->idconexion->set_charset(DB_CHARSET);
			$rs_query = $this->idconexion->query($sql);
			$resultado = array();
			if ($rs_query == false){ 
				//Si tengo un error en la SQL
				$resultado = $rs_query->error;
				if ($this->idconexion->error){
					self::triggerError($this->idconexion->error, $sql);
				}
			}else{
				while ( $fl_query = $rs_query->fetch_array(MYSQLI_BOTH)){
					$resultado[] = $fl_query[$numeroColumna];
				}
			}
			if ($this->slow_query == '1') {
				$queryTime2 = $this->getTimestamp();
				$logger = new logger();
				$logger->getTiempoQuery($queryTime1, $queryTime2, $sql);
				unset($logger);
			}
			self::desconectar();
			return $resultado;
		}	

		public function consultaRetornoColumnas($sql){
			self::conectar($sql);
			$this->idconexion->set_charset(DB_CHARSET);
			$rs_query = $this->idconexion->query($sql);
			$resultado = array();
			if ($this->idconexion->error){
				self::triggerError($this->idconexion->error, $sql);
			}else{
				while ( $fl_query = $rs_query->fetch_array(MYSQLI_ASSOC)){
					foreach ($fl_query as $key_query => $value_query) {
						$resultado["$key_query"][] = $value_query;
					}
				}
			}
			self::desconectar();
			if (isset($resultado)){
				return $resultado;
			}else{
				return null;
			}
		}	

		/**
         * Devuelve la primera fila en un array "NO" multi dimensional
		 * Algo similar a : [ 'usuario' => '1', 'nombre' => 'pepe', 'activo' => '0'  ]
         * 
         * @param string $sql: query sql a ejecutar
         * @param string: devuelve un string con el datos solicitado, en caso de no encontrar nada devuelve un string vacio
         */
		public function consultaFilaUnica($sql){
			self::conectar($sql);
			$this->idconexion->set_charset(DB_CHARSET);
			$rs_query = $this->idconexion->query($sql);
			$resultado = array();
			if ($this->idconexion->error){
				self::triggerError($this->idconexion->error, $sql);
			}else{
				while ( $fl_query = $rs_query->fetch_array(MYSQLI_ASSOC)){
					$resultado[] = $fl_query;
					break;
				}
			}
			self::desconectar();
			if (isset($resultado[0])){
				return $resultado[0];
			}else{
				return null;
			}
		}	

		/**
         * Devuelve el primer valor obtenido en la consulta, unicamente el primer valor STRING (no un array)
         * 
         * @param string $sql: query sql a ejecutar
         * @param string: devuelve un string con el datos solicitado, en caso de no encontrar nada devuelve un string vacio
         */
		public function consultaRetornoUnico($sql){
			self::conectar($sql);
			$this->idconexion->set_charset(DB_CHARSET);
			$rs_query = $this->idconexion->query($sql);
			$resultado = '';
			if ($this->idconexion->error){
				self::triggerError($this->idconexion->error, $sql);
			}else{
				if ( $fl_query = $rs_query->fetch_array(MYSQLI_BOTH)){
					$resultado = $fl_query[0];
				}
			}
			self::desconectar();
			return $resultado;
		}	

		public function consultaRetornoNFilas($sql){
			self::conectar($sql);
			$this->idconexion->set_charset(DB_CHARSET);
			$rs_query = $this->idconexion->query($sql);
			$n_filas = $rs_query->num_rows;
			self::desconectar();
			return $n_filas;
		}

		/**
		 * Función interna, lanza un trigger_error con el error de mysql que ha ocurrido ,
		 * para que aparezca en el log de error del apache, y envia un correo eletrónico con
		 * el error, en caso de estar marcado en config.
		 * 
		 * @param string $mensaje: mensaje de error a enviar
		 * @param string $sql: consulta sql que ha dado error
		 */
		public function triggerError($mensaje, $sql){
			trigger_error('MYSQL ERROR => '.$mensaje. '. SQL: '.$sql);
			/*$datos = array();
			$datos['type'] = 'MYSQL ERROR';
			$datos['error_msg'] = $mensaje;
			$datos['sql'] = $sql;
			if (ERROR_MAIL == '1'){
				$writer = new logger();
				$writer->sendMailError('Error Mysql', $datos);
				unset($writer);
			}*/
		}

		/**
		 * Devuelve el timestamp actual: Ej: 1537516032
		 * 
		 * @return timestamp de linux
		 */
		public function getTimestamp(){
			return time();
		}

		/**
		 * Usa la función logger de SIGO para escribir en un fichero de texto,
		 * Al similar a:
		 * [FECHA HORA] [PROCESO] [ID ASOCIADO] "Mensaje de logear"
		 * 
		 * @param string $nombreFichero: nombre del fichero en cual se escribe
		 * @param string $proceso: nombre del proceso el cual escribe, por si el mismo fichero tiene varios procesos escribiendo
		 * @param string $mensaje: Mensaje a escribir/loguear en el log
		 * @param int $idAsociado: id asociado (no obligatorio) es util para grepear
		 */
		public function writerLogger($nombreFichero, $proceso, $mensaje, $idAsociado = null){
			$writer = new logger();
			$writer->setId($idAsociado);
			$writer->loggerSigo($nombreFichero,  $proceso, $mensaje);
			unset($writer);
		}

	}

?>
