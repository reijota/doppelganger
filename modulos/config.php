<?php 
    #Si no existe la funcion "definir" definirla: define la constate si no existe evitando warnings php
    if (!function_exists('definir')){
        function definir($contante, $valor){
            if (!defined($contante)) {
                define($contante,$valor); 
            }
        }
    }

    #Proyecto
    definir('PROYECTO', 'DOPPELGANGER');
    #Datos de la base de datos
    definir('DB_HOST','localhost'); 
    definir('DB_USER','root'); 
    definir('DB_PASS',''); 
    definir('DB_NAME','doppelganger'); 
    definir('DB_CHARSET','utf8mb4'); 
    definir('SLOW_QUERY', 0); 
    
    #Datos del Usuario -> se aceder usando $_SESSION["usuario"] -> es un array con todos los datos de la tabla usuario + el nivel/rol
    #Google API key
    definir ('GOOGLEMPAPS_APIKEY', '');
    definir ('FACEAPI_KEY1', '');
    definir ('FACEAPI_KEY2', '');
    #Parametro para la versión del CRM
	definir('VERSION_CRM', '1.0.0');

?>
