<?php namespace dopel;

	class faceapi extends \core\base{

        private $apiKey1 = FACEAPI_KEY1;
        private $apiKey2 = FACEAPI_KEY2;
        private $tipoContenido;
        private $apiUrl = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/';


        /**
         * contrustor de clase
         * 
         * @param string $tipoContenido si es = externo, 
         */
        function __construct($tipoContenido = null ){
            $this->tipoContenido = ($tipoContenido == 'externo') ? 'application/json' : 'application/octet-stream' ;
        }

        public function getDatosImg($imagen){
            $retorno = $datosImagen =  false;
            
            // This sample uses the PHP5 HTTP_Request2 package
            // (http://pear.php.net/package/HTTP_Request2).
            
            
            $request = new \Http_Request2($this->apiUrl . '/detect');
            $url = $request->getUrl();
            $headers = array(
                'Content-Type' => $this->tipoContenido,
                'Ocp-Apim-Subscription-Key' => $this->apiKey1
            );
            $request->setHeader($headers);
            $parameters = array(
                // Request parameters
                'returnFaceId' => 'true',
                'returnFaceLandmarks' => 'false',
                'returnFaceAttributes' => 'age,gender,headPose,smile,facialHair,glasses,' .
                    'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise');
            $url->setQueryVariables($parameters);

            // Request body parameters
            if ($this->tipoContenido == 'application/json') {
                //$imagen = 'https://upload.wikimedia.org/wikipedia/commons/3/37/Dagestani_man_and_woman.jpg';
                $body = json_encode(array('url' => $imagen));
            } else {
                //$imagen='img.jpg';
                $body = fopen($imagen, 'rb');
            }
            $request->setBody($body);

            $request->setMethod(\HTTP_Request2::METHOD_POST);
            try
            {
                $response = $request->send();
                $datosImagen = json_decode($response->getBody());
                //echo json_encode(json_decode($response->getBody()), JSON_PRETTY_PRINT);
            }
            catch (HttpException $ex)
            {
                trigger_error("<pre>" . $ex . "</pre>");
            }
            if ($datosImagen) {
                $retorno = self::transformarDatos($datosImagen);
            }
            return $retorno;
        }

        public function transformarDatos($datosImagen){
            $datos['faceId'] = $datosImagen[0]->faceId;
            $datos['sonrisa'] = $datosImagen[0]->faceAttributes->smile; // 0 1 ??
            $datos['genero'] = self::getGenero($datosImagen[0]->faceAttributes->gender);
            $datos['edad'] = $datosImagen[0]->faceAttributes->age;
            $datos['bigote'] = $datosImagen[0]->faceAttributes->facialHair->moustache; //0 1;
            $datos['barba'] = $datosImagen[0]->faceAttributes->facialHair->beard;
            $datos['patillas'] = $datosImagen[0]->faceAttributes->facialHair->sideburns;
            $datos['gafas'] = $datosImagen[0]->faceAttributes->glasses;
            $datos['emocion'] = $datosImagen[0]->faceAttributes->emotion;
            $datos['ojosPintados'] = $datosImagen[0]->faceAttributes->makeup->eyeMakeup;
            $datos['labiosPintados'] = $datosImagen[0]->faceAttributes->makeup->lipMakeup;
            
            //brown, red, blond, black, gray, other
            $mayor = 0;
            $color = '';
            foreach ($datosImagen[0]->faceAttributes->hair->hairColor as $value) {
                if ($value->confidence > $mayor) {
                    $mayor = $value->confidence;
                    $color = $value->color;
                }
            }
            $datos['colorPelo'] = self::getColorPelo($color);
            return $datos;
        }

        public function getColorPelo($color){
            $retorno = false;
            switch ($color) {
                case 'brown':
                    $retorno = 'castaño';
                break;

                case 'red':
                    $retorno = 'rojo';
                break;

                case 'blond':
                    $retorno = 'rubio';
                break;

                case 'black':
                    $retorno = 'negro';
                break;

                case 'grey':
                    $retorno = 'canoso/gris';
                break;
            }

            return $retorno;
        }

        public function getGenero($genero){
            $retorno = false;
            switch ($genero) {
                case 'male':
                    $retorno = 'hombre';
                break;

                case 'female':
                    $retorno = 'mujer';
                break;
            }
            return $retorno;
        }
        
    }