<?php namespace dopel;

	class dopel extends \core\base{

        /**
		* Modificar un cmapo especifico de la hackeado -> Guardando en historico si corresponde
		* @param integer id de hackeado a modificar
		* @param string: campo a modificar
		* @param string: valor a modificar
		* @return mixed: false si falla la inserción o el número de filas insertadas si todo ok.
		*/
		public function modificarHackeado($idHackeado, $campo, $valor){
			$idHackeado = (int) $idHackeado;
			$valor = self::evitarScapeString($valor);
			$affected_rows = self::consultaSimple("UPDATE hackeado
                                                     SET `$campo` = '$valor', 
                                                     fechaActualizacion = CURRENT_TIMESTAMP 
                                                     WHERE id = '$idHackeado' ");
			return $affected_rows;
        }
        
        /**
         * Reemplazar comillas simples para evitar errores en la consulta 
         * 
         * @param string $valor 
         * @return string 
         */
        public function evitarScapeString($valor){
			$valor = str_replace("'", "´", $valor);
			return $valor;
		}

        /**
         * Inserta hackeado
         * 
         * @param string $nombre
         * 
         * @return int con el id de insercion;
         */
        public function insertarHack($nombre){
            $nombre = self::evitarScapeString($nombre);
            return self::consultaSimple("INSERT INTO `hackeado` SET
                    `nombre` = '$nombre'");
        }

        /**
		* Modificar un cmapo especifico de la datosFace -> Guardando en historico si corresponde
		* @param integer id de datosFace a modificar
		* @param string: campo a modificar
		* @param string: valor a modificar
		* @return mixed: false si falla la inserción o el número de filas insertadas si todo ok.
		*/
		public function modificarDatosFace($idHackeado, $campo, $valor){
			$idHackeado = (int) $idHackeado;
			$valor = self::evitarScapeString($valor);
			$affected_rows = self::consultaSimple("UPDATE datosFace
                                                     SET `$campo` = '$valor', 
                                                     fechaActualizacion = CURRENT_TIMESTAMP 
                                                     WHERE id = '$idHackeado' ");
			return $affected_rows;
        }

        /**
         * Inserta datosFace
         * 
         * @param string $nombre
         * 
         * @return int con el id de insercion;
         */
        public function insertarDatosFace($idHackeado){
            return self::consultaSimple("INSERT INTO `datosFace` SET
                    `idHackeado` = '$idHackeado'");
        }

        public function getDatosCuestionario($idHackeado){
            return self::consultaFilaUnica("SELECT * FROM hackeado WHERE id = {$idHackeado}");
        }

        public function getGenero($idHackeado){
            return self::consultaRetornoUnico("SELECT genero, count(*) as total FROM datosFace WHERE idHackeado = '{$idHackeado}' AND edad <> 0 GROUP BY GENERO ORDER BY total desc LIMIT 1;");
        }

        public function getEdad($idHackeado){
            return self::consultaRetornoUnico("SELECT ROUND(AVG(edad)) as edad FROM datosFace WHERE idHackeado = '{$idHackeado}' AND edad <> 0");
        }

        public function getColorPelo($idHackeado){
            return self::consultaRetornoUnico("SELECT colorPelo, count(*) as total FROM datosFace WHERE idHackeado = '{$idHackeado}' AND edad <> 0 GROUP BY colorPelo ORDER BY total desc LIMIT 1");
        }

        public function getGafas($idHackeado){
            return self::consultaRetornoUnico("SELECT gafas, count(*) as total FROM datosFace WHERE idHackeado = '{$idHackeado}' AND edad <> 0 GROUP BY gafas ORDER BY total desc LIMIT 1");
        }

        public function getBigote($idHackeado){
            return self::consultaRetornoUnico("SELECT ROUND(AVG(bigote)) as bigote FROM datosFace WHERE idHackeado = '{$idHackeado}' AND edad <> 0");
        }

        public function getBarba($idHackeado){
            return self::consultaRetornoUnico("SELECT ROUND(AVG(barba)) as barba FROM datosFace WHERE idHackeado = '{$idHackeado}' AND edad <> 0");
        }

        public function getImagen($idHackeado){
            return self::consultaRetornoUnico("SELECT imgNombre FROM datosFace WHERE idHackeado = '{$idHackeado}' AND imgNombre <> '' AND imgNombre IS NOT NULL LIMIT 1");
        }

        public function getListadoHacks(){
            return self::consultaRetorno("SELECT DISTINCT(f.idHackeado) as id, fechaCreacion, nombre, imgNombre, genero, edad, colorPelo FROM dopel.hackeado h INNER JOIN datosFace f On h.id = f.idHackeado ;");
        }
    }

    
