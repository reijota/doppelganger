<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Doppelganger</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= $app->base_url; ?>/web/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?= $app->base_url; ?>/web/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= $app->base_url; ?>/web/css/cover.css" rel="stylesheet">

    <link href="<?= $app->base_url; ?>/web/css/estilo.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?= $app->base_url; ?>/web/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="<?= $app->base_url; ?>/web/js/jquery-3.3.1.min.js"></script>
  <script>window.jQuery || document.write('<script src="web/js/jquery-3.3.1.min.js"><\/script>')</script>
  <script src="<?= $app->base_url; ?>/web/js/bootstrap.min.js"></script>
  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <script src="<?= $app->base_url; ?>/web/js/ie10-viewport-bug-workaround.js"></script>


  <script src="<?= $app->base_url; ?>/web/js/jquery.toaster.js"></script>  

</head>