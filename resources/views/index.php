<?php
    require_once('modulos/app.php');
    include ('resources/bloques/header.php');
?>

<head>
</head>

<body>

<div class="site-wrapper">

  <div class="site-wrapper-inner">

    <div class="cover-container">

      <div class="masthead clearfix">
        <div class="inner">
          <h3 class="masthead-brand"></h3>
          <nav>
            <ul class="nav masthead-nav">
              
            </ul>
          </nav>
        </div>
      </div>

      <div class="inner cover">
        <p>
        <img  src="web/img/Doppelganger-logotipo-center.png" class="imagenInicio" alt="Doppelganger" >
        </p>
        <p>
          <a href="questionario" class="btn btn-md btn-dopel mt-5">Entrar</a>
        </p>
      </div>

        <?php include('resources/bloques/pie.php'); ?>
      
    </div>

  </div>

</div>
</body>
</html>