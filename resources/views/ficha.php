<?php
    require_once('modulos/app.php');
    include ('resources/bloques/header.php');

    $dopel = new dopel\dopel();
    $datosQuestionario = $dopel->getDatosCuestionario($idHackeado);



    $genero = $dopel->getGenero($idHackeado);
    $edad = $dopel->getEdad($idHackeado);
    $colorPelo = $dopel->getColorPelo($idHackeado);
    $gafas = $dopel->getGafas($idHackeado);
    if ($gafas == 'NoGlasses') {
      $gafas = 'Sin gafas';
    }elseif($gafas == ''){
      $gafas = '';
    }else{
      $gafas = 'Con gafas';
    }
    $bigote = $dopel->getBigote($idHackeado);
    if ($bigote > 0.3) {
      $bigote = 'bigote';
    }else{
      $bigote = '';
    }
    $barba = $dopel->getBarba($idHackeado);
    if ($barba > 0.3) {
      $barba = 'barba';
    }else{
      $barba = '';
    }
    $imagen = $dopel->getImagen($idHackeado);

?>

<head>
  <script src="<?= $app->baseUrl('/web/js/jsFicha.js'); ?>"></script>
</head>

<body>

<div class="site-wrapper">

  <div class="site-wrapper-inner">

    <div class="cover-container2">

      <div class="masthead clearfix">
        <div class="inner">
          <h3 class="masthead-brand"></h3>
          <nav>
            <ul class="nav masthead-nav">
              
            </ul>
          </nav>
        </div>
      </div>

      <div class="inner cover">
      <p>
      <a href='../inicio'><img  src="<?= $app->base_url; ?>/web/img/Doppelganger-logotipo-center.png" class="logoPequeno" alt="Doppelganger" ></a>
        </p>
        
          <input type="hidden" id="idHackeado" name="idHackeado">
          <!-- capa wrapperPregunta1 -->
          <div id="wrapperPregunta1" class="">
            <p class="text-center"><?=$datosQuestionario['nombre']; ?></p>
            
            <p>

              <div class="row">
                <div class="col-sm">
                  <img src="<?= $app->base_url; ?>/web/img/capturas/<?=$imagen?>" alt="" class='imgCaptura' >
                </div>
                <div class="col-sm">
                  <p class="text-left"><?=$genero; ?></p>
                  <p class="text-left"><?php if($edad) echo "$edad años"; ?></p>
                  <p class="text-left"><?php if($colorPelo) echo "Pelo $colorPelo"; ?></p>
                  <p class="text-left"><?=$gafas; ?></p>
                  <p class="text-left">
                    <?php if ($genero == 'mujer') {
                       echo 'Maquillada'; 
                    }else{
                      echo "$bigote $barba";
                    }?></p>
                </div>
                <div class="col-sm">
                  <p class="text-left">Confianza <?=$datosQuestionario['ambiente']; ?>%</p>
                  <p class="text-left">Pasión <?=$datosQuestionario['virtual']; ?>%</p>
                  <p class="text-left">Empatia <?=$datosQuestionario['encajar']; ?>%</p>
                  <p class="text-left">Honestidad <?=$datosQuestionario['verdaderotu']; ?>%</p>
                  <p class="text-left">Intuicion <?=$datosQuestionario['teconoces']; ?>%</p>
                </div>
              </div>
              
              <button type="button" onclick="window.print()" data-wrapperactual="wrapperPregunta1" data-idinput="nombre" data-wrappersiguiente="wrapperPregunta2" class="btn btn-sm btn-dopel mt-5 btnDopel mr-5">Obtener identidad ></button>
              <a href="../listado"><button type="button" data-wrapperactual="wrapperPregunta1" data-idinput="nombre" data-wrappersiguiente="wrapperPregunta2" class="btn btn-sm btn-dopel mt-5 btnDopel ml-5">Cambiar identidad ></button></a>
            </p>
          </div>
          <!-- fin capa wrapperPregunta1 -->

        

      </div>

        <?php include('resources/bloques/pie.php'); ?>
      
    </div>

  </div>

</div>
</body>
</html>

