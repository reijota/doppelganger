<?php
    require_once('modulos/app.php');
    include ('resources/bloques/header.php');
	$app("cache")->clear();
?>

<head>
  <script src="<?= $app->baseUrl('/web/js/jsQuestionario.js'); ?>"></script>
</head>

<body>

<div class="site-wrapper">

  <div class="site-wrapper-inner">

    <div class="cover-container">

      <div class="masthead clearfix">
        <div class="inner">
          <h3 class="masthead-brand"></h3>
          <nav>
            <ul class="nav masthead-nav">
              
            </ul>
          </nav>
        </div>
      </div>

      <div class="inner cover">
        <p>
        <a href='inicio'><img  src="web/img/Doppelganger-logotipo-center.png" class="logoPequeno" alt="Doppelganger" ></a>
        </p>
        <form id="questionarioForm" name="questionarioForm" enctype="multipart/form-data">
          <input type="hidden" id="idHackeado" name="idHackeado">
          <!-- capa wrapperPregunta1 -->
          <div id="wrapperPregunta1" class="preguntas">
            <p>
            1-Hola, ¿quién eres?
            </p>
            <p><input type="text" id="nombre" name="nombre" class="form-control"></p>
            <p>
              <button type="button" data-wrapperactual="wrapperPregunta1" data-idinput="nombre" data-wrappersiguiente="wrapperPregunta2" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta1 -->

          <!-- capa wrapperPregunta2 -->
          <div id="wrapperPregunta2" class="preguntas">
            <p>
            2-Sería un placer hablar contigo <b><span id="wrapperNombre"></span></b>, ¿quieres charlar?
            </p>
            <p>
              <select name="charlar" id="charlar" class="form-control">
                <option value=""></option>
                <option value="si">Si</option>
                <option value="no">No</option>
              </select>  
            <p>
              <button type="button" id="inicio" data-wrapperactual="wrapperPregunta2"  data-idinput="charlar"  data-wrappersiguiente="wrapperPregunta3" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta2 -->

          <!-- capa wrapperPregunta3 -->
          <div id="wrapperPregunta3" class="preguntas">
            <p>
              3-A veces me pregunto cómo sería todo  si hubiera crecido en un ambiente distinto, ¿tú crees que hubieses sido una persona diferente?  
            </p>
            <p>
              <select name="ambiente" id="ambiente" class="form-control">
                <option value=""></option>
                <option value="50">Creo que si, el contexto claramente influye</option>
                <option value="90">Hubiese sido la misma persona, somos una construcción biológica, no social</option>
                <option value="10">No lo sé, puede ser o quizás no</option>
              </select>  
            <p>
            <p>
              <button type="button" id="entrar" data-idinput="ambiente" data-wrapperactual="wrapperPregunta3" data-wrappersiguiente="wrapperPregunta4" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta3 -->

          <!-- capa wrapperPregunta4 -->
          <div id="wrapperPregunta4" class="preguntas">
            <p>
              4-Puede ser...muchas veces siento que no encajo, ¿a ti te gusta la sociedad a la cual perteneces?
            </p>
            <p>
              <select name="encajar" id="encajar" class="form-control">
                <option value=""></option>
                <option value="10">Sí, creo que es necesaria tal y como esta construída</option>
                <option value="90">No, es injusta y desigual, cambiaría practicamente todo</option>
                <option value="50">Hay cosas que me gustan y otras que detesto</option>
              </select>  
            <p>
            <p>
              <button type="button" id="entrar" data-idinput="encajar" data-wrapperactual="wrapperPregunta4" data-wrappersiguiente="wrapperPregunta5" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta4 -->

          <!-- capa wrapperPregunta5 -->
          <div id="wrapperPregunta5" class="preguntas">
            <p>
              5-Me gusta pensar en mí como un "hacer virtual" no como un "ser virtual", ¿tú te identificas por las cosas que haces?
            </p>
            <p>
              <select name="virtual" id="virtual" class="form-control">
                <option value=""></option>
                <option value="90">Si, totalmente</option>
                <option value="10">NO,  mis hechos  no son reflejo de mi ser</option>
                <option value="50">Quizás, no suelo pensar en ello</option>
              </select>  
            <p>
            <p>
              <button type="button" id="entrar" data-idinput="virtual" data-wrapperactual="wrapperPregunta5" data-wrappersiguiente="wrapperPregunta6" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta5 -->


          <!-- capa wrapperPregunta6 -->
          <div id="wrapperPregunta6" class="preguntas">
            <p>
              6-Entonces ¿crees que tu apariencia es una representación justa del “verdadero tu”?
            </p>
            <p>
              <select name="verdaderotu" id="verdaderotu" class="form-control">
                <option value=""></option>
                <option value="10">Si, asi es, soy tal cual me ves</option>
                <option value="50">No, para nada. Mi apariencia es pura fachada</option>
                <option value="90">Muestro lo que quiero que los demás interpreten</option>
              </select>  
            <p>
            <p>
              <button type="button" id="entrar" data-idinput="verdaderotu" data-wrapperactual="wrapperPregunta6" data-wrappersiguiente="wrapperPregunta7" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta6 -->


          <!-- capa wrapperPregunta7 -->
          <div id="wrapperPregunta7" class="preguntas">
            <p>
              7-¿A sí que crees que te conoces bien?
            </p>
            <p>
              <select name="teconoces" id="teconoces" class="form-control">
                <option value=""></option>
                <option value="90">Si, quien no</option>
                <option value="10">No, para nada. A veces no me reconozco</option>
                <option value="50">Puede ser, no tengo plena confianza en ello</option>
              </select>  
            <p>
            <p>
              <button type="button" id="entrar" data-idinput="teconoces" data-wrapperactual="wrapperPregunta7" data-wrappersiguiente="wrapperPregunta8" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta7 -->


          <!-- capa wrapperPregunta8 -->
          <div id="wrapperPregunta8" class="preguntas">
            <p>
              8-Alguna vez te has planteado ser otra persona diferente, poder elegir... ¿qué  te gustaría cambiar? 
            </p>
            <p>
              <select name="serotro" id="serotro" class="form-control">
                <option value=""></option>
                <option value="50">Sí, cambiaría de sexo</option>
                <option value="90">No cambiaría nada. Me usta como soy</option>
                <option value="10">Me gustaría poder elegir, seguramente cambiaría muchas cosas</option>

              </select>  
            <p>
            <p>
              <button type="button" id="entrar" data-idinput="serotro" data-wrapperactual="wrapperPregunta8" data-wrappersiguiente="fin" class="btn btn-sm btn-dopel mt-5 btnDopel">Siguiente</button>
            </p>
          </div>
          <!-- fin capa wrapperPregunta8 -->

          <!-- capa fin -->
          <div id="fin" class="preguntas">
            <p>
              Hackeando identidad...
            </p>
            <p>
            <div id="loader"></div>
            <p>

          </div>
          <!-- fin capa fin -->
        </form>
        

      </div>

        <?php include('resources/bloques/pie.php'); ?>
      
    </div>

  </div>

</div>
</body>
</html>

