<?php
    require_once('modulos/app.php');
    include ('resources/bloques/header.php');
    $dopel = new dopel\dopel();

	$listado = $dopel->getListadoHacks();
?>

<head>
  <link rel="stylesheet" type="text/css" href="<?= $app->baseUrl('/web/css/datatables.min.css')?>"/>
 
  <script type="text/javascript" src="<?= $app->baseUrl('/web/js/datatables.min.js')?>"></script>
  <script src="<?= $app->baseUrl('/web/js/jsListado.js'); ?>"></script>
  <style>
body {
    color: #6e6e6e;
    text-align: center;
    text-shadow: 0 1px 3px rgba(0,0,0,.5);
}
table.dataTable.display tbody tr.odd {
    background-color: #1b1b1b;
}
table.dataTable tbody tr {
    background-color: #000000;
}

table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
    background-color: #1b1b1b;
}

table.dataTable.display tbody tr.even>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1 {
    background-color: #000000;
}
  </style>
</head>


<body>

<div class="site-wrapper">

  <div class="site-wrapper-inner">

    <div class="cover-container">

      <div class="masthead clearfix">
        <div class="inner">
          <h3 class="masthead-brand"></h3>
          <nav>
            <ul class="nav masthead-nav">
              
            </ul>
          </nav>
        </div>
      </div>

      <div class="inner cover">
        <p>
        <a href='inicio'><img  src="web/img/Doppelganger-logotipo-center.png" class="logoPequeno" alt="Doppelganger" ></a>
        </p>

          <input type="hidden" id="idHackeado" name="idHackeado">
          <!-- capa wrapperPregunta1 -->
          <div id="wrapperPregunta1" class="">
          <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Fecha Creacion</th>
                <th>Nombre</th>
                <th>Edad</th>
                <th>Sexo</th>
                <th>Foto/enlace</th>
            </tr>
        </thead>
        <tbody>
            <?php
              foreach ($listado as $sujeto) {
                echo "<tr><td>{$sujeto["fechaCreacion"]}</td>";
                echo "<td>{$sujeto["nombre"]}</td>";
                echo "<td>{$sujeto["edad"]}</td>";
                echo "<td>{$sujeto["genero"]}</td>";
                echo "<td><a href='ficha/{$sujeto["id"]}'><img src='web/img/capturas/{$sujeto["imgNombre"]}' style='height:20px'></td></tr>";
              }
            ?>
        </tbody>

    </table>
          </div>
          <!-- fin capa wrapperPregunta1 -->

          

      </div>

        <?php include('resources/bloques/pie.php'); ?>
      
    </div>

  </div>

</div>
</body>
</html>

