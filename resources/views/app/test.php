
<?php
$ocpApimSubscriptionKey = '03be468fded846d4aebaa6bbce514598';
$uriBase = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0/';

$image='img.jpg';
$fp = fopen($image, 'rb');

// This sample uses the PHP5 HTTP_Request2 package
// (http://pear.php.net/package/HTTP_Request2).
require_once 'http/Request2.php';
$request = new Http_Request2($uriBase . '/detect');
$url = $request->getUrl();
$headers = array(
    'Content-Type' => 'application/octet-stream',
    'Ocp-Apim-Subscription-Key' => $ocpApimSubscriptionKey
);
$request->setHeader($headers);
$parameters = array(
    // Request parameters
    'returnFaceId' => 'true',
    'returnFaceLandmarks' => 'false',
    'returnFaceAttributes' => 'age,gender,headPose,smile,facialHair,glasses,' .
        'emotion,hair,makeup,occlusion,accessories,blur,exposure,noise');
$url->setQueryVariables($parameters);

// Request body parameters
//$body = json_encode(array('url' => $encoded_image));
//$body = $encoded_image;
// Request body
//$request->setBody($body);

$request->setBody($fp);
$request->setMethod(HTTP_Request2::METHOD_POST);
try
{
    $response = $request->send();
    $respuesta = json_decode($response->getBody());
    //echo json_encode(json_decode($response->getBody()), JSON_PRETTY_PRINT);
}
catch (HttpException $ex)
{
    echo "<pre>" . $ex . "</pre>";
}

$faceId = $respuesta[0]->faceId;
$sonrisa = $respuesta[0]->faceAttributes->smile; // 0 1 ??
$genero = $respuesta[0]->faceAttributes->gender;
$edad = $respuesta[0]->faceAttributes->age;
$bigote = $respuesta[0]->faceAttributes->facialHair->moustache; //0 1;
$barba = $respuesta[0]->faceAttributes->facialHair->beard;
$patillas = $respuesta[0]->faceAttributes->facialHair->sideburns;
$gafas = $respuesta[0]->faceAttributes->glasses;
$emocion = $respuesta[0]->faceAttributes->emotion;
$ojosPintados = $respuesta[0]->faceAttributes->makeup->eyeMakeup;
$labiosPintados = $respuesta[0]->faceAttributes->makeup->lipMakeup;
//print_r($respuesta[0]->faceAttributes->hair->hairColor);
//brown, red, blond, black, gray, other
$mayor = 0;
$color = '';
foreach ($respuesta[0]->faceAttributes->hair->hairColor as $value) {
    if ($value->confidence > $mayor) {
        $mayor = $value->confidence;
        $color = $value->color;
    }
    //echo ($value->color." ".$value->confidence);
}
echo "color = $color";
if($sonrisa == 0){
    $sonrie = "no sonrie";
}else{
    $sonrie = "esta sonriendo";
}
echo "\n\nes $genero de $edad años $sonrie\n\n";

//echo "brown:$brown, red: $red, blond: $blond, black: $black, gray: $gray, other: $other";
?>