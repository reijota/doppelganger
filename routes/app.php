<?php

    $app->get("/", function() {
        return $this->render("resources/views/index.php");
    });
    #Esta ruta la duplico de momento hasta sustiur todas las referencias a index.php en el código
    $app->get("/inicio", function() {
        return $this->render("resources/views/index.php");
    });

    $app->get("/questionario", function() {
        return $this->render("resources/views/questionario.php");
    });

    $app->get("/listado", function() {
        return $this->render("resources/views/listado.php");
    });

    $app->get("/ficha/:idHackeado", function($params) {
        return $this->render("resources/views/ficha.php", 
        ["idHackeado" => $params["idHackeado"] ]);
    });

?>