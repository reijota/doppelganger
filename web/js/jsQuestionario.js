$(document).ready(function () {
    
    $(".btn-dopel").click(function (e) { 
        var wrapperactual = $(this).data("wrapperactual"),
            wrappersiguiente = $(this).data("wrappersiguiente"),
            idInput = $(this).data("idinput");
        var valor = $("#" + idInput).val();
        
        if (wrappersiguiente == 'fin') {
            idInput = wrappersiguiente;
        }

        if (validarRespuesta(valor, wrapperactual, wrappersiguiente)){
            condicionRespuesta(idInput, valor);
        }

    });

    $(".preguntas").hide();
    $("#wrapperPregunta1").show();
    //preguntas
});

function validarRespuesta(valor, wrapperactual, wrappersiguiente){
    var retorno;
    if (valor) {
        $("#" + wrapperactual).fadeOut( 400, function() {
            $("#" + wrappersiguiente).fadeIn();
            });
        retorno = true;
    } else{
        $.toaster({ message : 'Rellena el formaulario antes de continuar', title : 'Advertencia', priority : 'danger' });
        $.toaster('Rellena el formaulario antes de continuar', 'Incompleto', 'danger');
        retorno = false;
    }
    return retorno;
}

function condicionRespuesta(idInput, valor){
    switch (idInput) {
        case 'nombre':
            $("#wrapperNombre").text(valor);
            $.post('resources/ajax/gestionQuiestionario.php', {
                accion: 'insertarNombre',
                nombre: valor
            }).done(function(data){
                $("#idHackeado").val(data);
                //capturar sujeto
                $.post('resources/ajax/gestionQuiestionario.php', {
                    accion: 'capturar',
                    idHackeado: data
                }).done(function(dataSujeto){
                    console.log("qwerty");
                    console.log(dataSujeto);
                    
                }); 



            }); 
            
        break;

        case 'ambiente':
        case 'virtual':
        case 'verdaderotu':
        case 'serotro':
            var idHackeado = $("#idHackeado").val();
            $.post('resources/ajax/gestionQuiestionario.php', {
                accion: 'capturar',
                idHackeado: idHackeado
            }).done(function(dataSujeto){
                console.log("qwerty");
                console.log(dataSujeto);
                
            }); 

        break;

        case 'charlar':
            if (valor == 'no') {
                location.href ="inicio";
            }
        break;

        case 'fin':
            var formData = new FormData(document.getElementById('questionarioForm'));
            formData.append("accion", "guardarFormulario");
            console.log(formData);
            $.ajax({
                type: 'POST',
                url: "resources/ajax/gestionQuiestionario.php",
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log(response);
                    finalizar();
                }
            });
            
        break;
    
        default:
            break;
    }
}

var myVar;

function finalizar() {
    myVar = setTimeout(showPage, 7000);
}

function showPage() {
	var idHackeado = $("#idHackeado").val();
	location.href="ficha/" + idHackeado;
}
